package cat.itb.nyanmaps

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cat.itb.nyanmaps.model.NyanMarker
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage

class MarkerViewModel: ViewModel() {
    private var markersList: ArrayList<NyanMarker> = arrayListOf<NyanMarker>()
    val currentMarker = MutableLiveData<NyanMarker>()
    val markers = MutableLiveData<MutableList<NyanMarker>>()
    var filteredMarkers = MutableLiveData<MutableList<NyanMarker>>()
    private var savedMarker: NyanMarker?
    private var db = FirebaseFirestore.getInstance()
    var currentTagFilter = Tag.values()[0]

/*    init {
        savedMarker = null
        currentMarker.value = NyanMarker()
        markers.value = mutableListOf()
        db = FirebaseFirestore.getInstance()
        db.collection("nyan_markers").get().addOnSuccessListener {
            val newMarkers: MutableList<NyanMarker> = mutableListOf()

            it.forEach { results ->
                if (results != null) {
                    newMarkers.add(
                        NyanMarker(
                            results["latitude"] as Double,
                            results["longitude"] as Double,
                            imagePath = Uri.parse(results["image_path"] as String)
                        )
                    )
                }
            }
            markers.postValue(newMarkers)
        }
    }*/

    fun addToList(m:NyanMarker){
        markersList.add(m)
    }
    init {
        savedMarker = null
        markersList.add(NyanMarker(id = null, latitude = (41.4534).toFloat(), longitude = (2.1841).toFloat(),color= BitmapDescriptorFactory.HUE_BLUE,name= "ITB", image = null))
    }

    fun resetSave(){
        savedMarker = null
    }

    fun setTagFilter(tag: Tag) {
        println("Setting tag filter to $tag")
        currentTagFilter = tag
        updateFilteredMarkers()
    }

    fun updateFilteredMarkers() {
        val filteredMarkers = mutableListOf<NyanMarker>()
        markers.value?.forEach {
            if (it.tag == currentTagFilter) {
                filteredMarkers.add(it)
            }
        }

        this.filteredMarkers.postValue(filteredMarkers)
        println("Filtered markers: $filteredMarkers")
    }

   /* fun addMarker(nyanMarker: NyanMarker) {

         db = FirebaseFirestore.getInstance()
        val file = nyanMarker.imagePath
        val storage = FirebaseStorage.getInstance().reference

        val childRef = storage.child("images/${file.lastPathSegment}")

        val uploadTask = childRef.putFile(file)
        val urlTask = uploadTask.continueWithTask { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
            }
            childRef.downloadUrl
        }.addOnCompleteListener { task ->
            if(task.isComplete){
                nyanMarker.imagePath = task.result
                db.collection("nyan_markers").add(
                    hashMapOf(
                        "latitude" to nyanMarker.latitude,
                        "longitude" to nyanMarker.longitude,
                        "image_path" to nyanMarker.imagePath.toString()
                    )
                )
                markers.value?.add(nyanMarker.copy())
                updateFilteredMarkers()
            }
        }

    }*/
    fun saveMarker (m:NyanMarker):Boolean{
        if(savedMarker == null){
            savedMarker = m
            return true
        }
        return false
    }

    fun getSavedMarker():NyanMarker? = savedMarker

    fun saveImageSavedMarker(uriImage: String){
        savedMarker!!.image = uriImage
    }

    fun saveImageData(title:String, latitude:Float, longitude:Float, color:Float){
        savedMarker!!.latitude = latitude
        savedMarker!!.longitude = longitude
        savedMarker!!.name = title
        savedMarker!!.color = color
    }

    fun textFromValue(colorVal:Float):String{
        when (colorVal){
            BitmapDescriptorFactory.HUE_BLUE -> return "Blue"
            BitmapDescriptorFactory.HUE_VIOLET -> return "Violet"
            else -> return "Violet"
        }
    }

}