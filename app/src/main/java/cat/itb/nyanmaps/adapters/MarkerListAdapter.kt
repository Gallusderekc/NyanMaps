package cat.itb.nyanmaps.adapters

import android.content.Context
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import cat.itb.nyanmaps.MarkerOnClickListener
import cat.itb.nyanmaps.R
import cat.itb.nyanmaps.databinding.MarkerItemBinding
import cat.itb.nyanmaps.fragments.ListFragmentDirections
import cat.itb.nyanmaps.model.NyanMarker
import com.google.android.gms.maps.model.BitmapDescriptorFactory

class MarkerListAdapter(
    private var nyanMarker_list: List<NyanMarker>,
    ): RecyclerView.Adapter<MarkerListAdapter.ViewHolder>() {

    lateinit var binding : MarkerItemBinding
    private lateinit var context: Context

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun binding(nyanMarker: NyanMarker) {
/*            binding.root.setOnClickListener {
                listenerTag.onClick(nyanMarker)
            }*/
            binding.markerTitle.text = nyanMarker.name
            binding.CoordinadesText.text = "Latitude: ${nyanMarker.latitude} Longitude: ${nyanMarker.longitude}"
            binding.image.setImageResource(selectImage(nyanMarker.color))
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.marker_item, parent, false)
        binding = MarkerItemBinding.bind(view)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val marker = nyanMarker_list[position]
        /*with(holder) {
            setListener(marker)
            binding.latitudeText.text = marker.latitude.toString()
            binding.latitudeText.text = marker.longitude.toString()
            binding.image.setImageBitmap(BitmapFactory.decodeFile(marker.imagePath.path))
            binding.image.rotation = 90f
        }*/

        holder.setIsRecyclable(false)
        holder.binding(marker)
        holder.itemView.setOnClickListener{
            val directions = ListFragmentDirections.actionListFragmentToMarkerEdit(
                marker.id!!
            )
            it.findNavController().navigate(directions)
            //view -> view.findNavController().navigate(R.id.action_markerList_to_markerEdit)
        }
    }
    fun setMarkerList(m:List<NyanMarker>){
        nyanMarker_list = m
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return nyanMarker_list.size
    }
    private fun selectImage(color: Float?): Int {
        when(color){
            BitmapDescriptorFactory.HUE_BLUE -> return R.drawable.cookie8bit
            BitmapDescriptorFactory.HUE_VIOLET -> return R.drawable.market
            else -> return R.drawable.pizza
        }
    }
}