package cat.itb.nyanmaps.model

import android.net.Uri
import androidx.core.net.toUri
import cat.itb.nyanmaps.Tag
import com.google.android.gms.maps.model.BitmapDescriptorFactory

data class NyanMarker(

    var id: String? = null,
    var latitude: Float? = null,
    var longitude: Float? = null,
    val restaurant: String? = null,
    var color:Float? = BitmapDescriptorFactory.HUE_BLUE,
    var name:String? = "Title",
    var image: String? = null,
    var tag: Tag = Tag.Nyan

)