package cat.itb.nyanmaps

import cat.itb.nyanmaps.model.NyanMarker

interface MarkerOnClickListener {
    fun onClick(nyanMarker: NyanMarker)
}