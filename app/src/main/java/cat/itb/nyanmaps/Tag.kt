package cat.itb.nyanmaps

enum class Tag {
    Nyan,
    Pizza,
    Potatoes
}