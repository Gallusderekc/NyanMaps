package cat.itb.nyanmaps.fragments.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.motion.widget.Debug
import androidx.navigation.fragment.findNavController
import cat.itb.nyanmaps.databinding.FragmentRegisterBinding
import com.google.firebase.auth.FirebaseAuth

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 * Use the [RegisterFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RegisterFragment : Fragment() {

    private lateinit var binding: FragmentRegisterBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentRegisterBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val email = arguments?.getString("email")

        binding.haveAnAccountText.setOnClickListener{
            val email = binding.editTextTextEmailAddress.text.toString()
            gotoLogIn(email)
        }
        binding.continueButton.setOnClickListener {
            val email = binding.editTextTextEmailAddress.text.toString()
            val password = binding.editTextTextPassword.text.toString()
            FirebaseAuth.getInstance()
                .createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        val emailLogged = it.result?.user?.email
                        gotoLogIn(emailLogged!!)
                    }
                    else{
                        println("Error al registrar l'usuari")
                    }
                }

        }
    }

    private fun gotoLogIn(emailLogged: String) {
        val directions = RegisterFragmentDirections.actionRegistrerFragmentToLoginFragment(emailLogged)
        findNavController().navigate(directions)
    }

}