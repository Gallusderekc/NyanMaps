package cat.itb.nyanmaps.fragments.login

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.edit
import androidx.navigation.fragment.findNavController
import cat.itb.nyanmaps.MainActivity
import cat.itb.nyanmaps.databinding.FragmentLoginBinding
import com.google.firebase.auth.FirebaseAuth

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [LogInFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class LogInFragment : Fragment() {
    private lateinit var binding: FragmentLoginBinding
    lateinit var myPreferences: SharedPreferences
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentLoginBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //myPreferences = requireActivity().getSharedPreferences("MySharedPreferences", Context.MODE_PRIVATE)
        myPreferences = activity?.getPreferences(Context.MODE_PRIVATE)!!
        var email = myPreferences.getString("email", "")
        var emailRegister = arguments?.getString("email")
        var passText = myPreferences.getString("password", "")
        var rememberState = myPreferences.getBoolean("remember", false)
        //val email = arguments?.getString("email")
        if(emailRegister!=null){
            binding.editTextTextEmailAddress.setText(emailRegister)
        }else{
            if(email!=null){
                if(email.isNotEmpty()){
                    binding.editTextTextEmailAddress.setText(email)
                    binding.editTextTextPassword.setText(passText)
                    binding.switch1.isChecked = rememberState
                }
            }
        }

        binding.textView3.setOnClickListener{
            gotoRegister(binding.editTextTextEmailAddress.text.toString())
        }
        binding.continueButton.setOnClickListener{
            email = binding.editTextTextEmailAddress.text.toString()
            passText = binding.editTextTextPassword.text.toString()
            FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(email!!, passText!!)
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        if(binding.switch1.isChecked){
                            myPreferences.edit {
                                putString("email", email)
                                putString("password", passText)
                                putBoolean("remember", binding.switch1.isChecked)
                                apply()
                            }
                            println("Saved preferences")
                        }else{
                            myPreferences.edit {
                                putString("email", "")
                                putString("password", "")
                                putBoolean("remember", false)
                                apply()
                            }
                        }
                        val intent = Intent(requireContext(), MainActivity::class.java)
                        startActivity(intent)
                    }
                    else{
                        println("Error al fer login")
                        binding.editTextTextPassword.setText("")
                        Toast.makeText(context, "User or Password not correct", Toast.LENGTH_SHORT).show()
                    }
                }
        }
    }
    private fun gotoRegister(emailLogged: String) {
        val directions = LogInFragmentDirections.actionLoginFragmentToRegistrerFragment(emailLogged)
        findNavController().navigate(directions)

    }
}