package cat.itb.nyanmaps.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import cat.itb.nyanmaps.adapters.MarkerListAdapter
import cat.itb.nyanmaps.databinding.FragmentListBinding
import cat.itb.nyanmaps.model.NyanMarker
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.firebase.firestore.*
import cat.itb.nyanmaps.R

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ListFragment : Fragment() {
    // TODO: Rename and change types of parameters


    lateinit var binding: FragmentListBinding
    private lateinit var markerListAdapter: MarkerListAdapter
    //private val viewModel: MarkerViewModel by activityViewModels()
    private val db = FirebaseFirestore.getInstance()
    private lateinit var markerList :ArrayList<NyanMarker>
    private var selectedColor: Float? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentListBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

/*        viewModel.filteredMarkers.value?.let {
            setUpRecyclerView(it)
        }
        viewModel.filteredMarkers.observe(viewLifecycleOwner, Observer {
            if (viewModel.filteredMarkers.value == null) {
                Toast.makeText(context, "ERROR", Toast.LENGTH_SHORT).show()
            } else {
                setUpRecyclerView(it)
            }
        })
        binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (!binding.recyclerView.canScrollVertically(1)) {
                    //viewModel.fetchData("next")
                }
            }
        })
        binding.swipeRefresh.setOnRefreshListener {
            binding.swipeRefresh.isRefreshing = false
        }
        binding.tagSpinner.adapter =
            ArrayAdapter(this.requireContext(), android.R.layout.simple_spinner_item, Tag.values())
        binding.tagSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                viewModel.setTagFilter(binding.tagSpinner.selectedItem as Tag)
            }
        }*/


        setUpRecyclerView(arrayListOf())

        val listColors = resources.getStringArray(R.array.color_spinner_recycler)
        val adapterSpinner = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, listColors)
        binding.colorselector.adapter = adapterSpinner
        binding.colorselector.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                when(listColors[p2]){
                    "Violet"-> {
                        selectedColor= BitmapDescriptorFactory.HUE_VIOLET
                    }
                    "Blue"-> {
                        selectedColor = BitmapDescriptorFactory.HUE_BLUE
                    }
                    "All"->{
                        selectedColor = null
                    }
                }
                eventChangeListener(selectedColor)
                //println(markerColor)
            }
            override fun onNothingSelected(p0: AdapterView<*>?) {
                selectedColor = null
                eventChangeListener(selectedColor)
                //println(markerColor)
            }
        }
    }


    private fun eventChangeListener(color:Float?) {
        //markerAdapter.clearMarkerList()
        markerList = arrayListOf<NyanMarker>()
        if(color == null){
            db.collection("markers").addSnapshotListener(object: EventListener<QuerySnapshot> {
                override fun onEvent(value: QuerySnapshot?, error: FirebaseFirestoreException?) {
                    if(error != null){
                        Log.e("Firestore error", error.message.toString())
                        return
                    }
                    for(dc: DocumentChange in value?.documentChanges!!){
                        if(dc.type == DocumentChange.Type.ADDED){
                            val newMarker = dc.document.toObject(NyanMarker::class.java)
                            newMarker.id = dc.document.id
                            markerList.add(newMarker)
                        }
                    }
                    markerListAdapter.setMarkerList(markerList.toList())
                }
            })
        }else{
            println(color.toInt())
            db.collection("markers").whereEqualTo("color", color).addSnapshotListener(object:
                EventListener<QuerySnapshot> {
                override fun onEvent(value: QuerySnapshot?, error: FirebaseFirestoreException?) {
                    if(error != null){
                        Log.e("Firestore error", error.message.toString())
                        return
                    }
                    for(dc: DocumentChange in value?.documentChanges!!){
                        if(dc.type == DocumentChange.Type.ADDED){
                            val newMarker = dc.document.toObject(NyanMarker::class.java)
                            newMarker.id = dc.document.id
                            markerList.add(newMarker)
                        }
                    }
                    markerListAdapter.setMarkerList(markerList.toList())
                }
            })
        }
    }

    fun setUpRecyclerView(nyanMarkers: List<NyanMarker>) {
/*        MarkerListAdapter = MarkerListAdapter(nyanMarkers, this)
        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = MarkerListAdapter
        }*/
        markerListAdapter = MarkerListAdapter(nyanMarkers)
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = markerListAdapter
        }
    }

/*    override fun onClick(nyanMarker: NyanMarker) {
        TODO("Not yet implemented")
    }*/
}