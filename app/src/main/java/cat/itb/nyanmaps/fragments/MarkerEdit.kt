package cat.itb.nyanmaps.fragments

import android.app.AlertDialog
import android.content.DialogInterface
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import cat.itb.nyanmaps.MarkerViewModel
import cat.itb.nyanmaps.R
import cat.itb.nyanmaps.databinding.FragmentMarkerEditBinding
import cat.itb.nyanmaps.model.NyanMarker
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.File

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [MarkerEdit.newInstance] factory method to
 * create an instance of this fragment.
 */
class MarkerEdit : Fragment() {
    private val viewmodel: MarkerViewModel by activityViewModels()
    private lateinit var binding: FragmentMarkerEditBinding
    private val db = FirebaseFirestore.getInstance()
    private lateinit var editableMarker : NyanMarker

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentMarkerEditBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val marker_id = arguments?.getString("id_marker")

        db.collection("markers").document(marker_id!!).get().addOnSuccessListener {
            //println((it.get("lon") as Double).toFloat())
            editableMarker = NyanMarker(longitude =(it.get("lon") as Double).toFloat(), latitude = (it.get("lat") as Double).toFloat(), name = it.get("name") as String, color = (it.get("color") as Double).toFloat(), image = "")
            binding.titleName.setText(editableMarker.name!!)
            binding.coordenades.text = "[${String.format("%.4f", editableMarker.latitude)}, ${String.format("%.4f", editableMarker.longitude)}]"
            val listColors = resources.getStringArray(R.array.color_spinner)
            val adapterSpinner = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, listColors)
            binding.colorSelect.adapter = adapterSpinner
            binding.colorSelect.setSelection(adapterSpinner.getPosition(viewmodel.textFromValue(editableMarker.color!!)))
            binding.colorSelect.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    when(listColors[p2]){
                        "Yellow"-> editableMarker.color = BitmapDescriptorFactory.HUE_YELLOW
                        "Red"-> editableMarker.color = BitmapDescriptorFactory.HUE_RED
                        "Orange"-> editableMarker.color = BitmapDescriptorFactory.HUE_ORANGE
                        "Purple"-> editableMarker.color = BitmapDescriptorFactory.HUE_MAGENTA
                    }
                    //println(markerColor)
                }
                override fun onNothingSelected(p0: AdapterView<*>?) {
                    println(editableMarker.color)
                }
            }
            val storage = FirebaseStorage.getInstance().reference.child("images/${marker_id}")
            val localFile = File.createTempFile("temp", "jpeg")
            storage.getFile(localFile).addOnSuccessListener {
                val bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
                binding.cameraImage.rotation=90f
                binding.cameraImage.setImageBitmap(bitmap)
            }.addOnFailureListener{
                Toast.makeText(context, "Error downloading image!", Toast.LENGTH_SHORT)
                    .show()
            }
            binding.acceptButton.setOnClickListener{
                editableMarker.name = binding.titleName.text.toString()
                db.collection("markers").document(marker_id).set(editableMarker).addOnSuccessListener {
                    println("Marker updated")
                    navigateBacktoList()
                }.addOnFailureListener { e ->
                    Log.w("ERROR", "Error creant el marcador", e)
                }
            }
            binding.cancelButton.setOnClickListener{
                navigateBacktoList()
            }
            binding.deleteMarkerButton.setOnClickListener{
                println("Button pressed")
                val alertDialog = AlertDialog.Builder(context)
                alertDialog
                    .setTitle("Confirmation")
                    .setMessage("Are you sure you want to delete this marker?")
                    .setPositiveButton("Yes", DialogInterface.OnClickListener{ dialog, id ->
                        if(deleteThisMarker(marker_id)){
                            Toast.makeText(context, "Marker deleted!", Toast.LENGTH_SHORT)
                                .show()
                            navigateBacktoList()
                        }else{
                            Toast.makeText(context, "Marker still alive!", Toast.LENGTH_SHORT)
                                .show()
                        }
                    })
                    .setNegativeButton("No", DialogInterface.OnClickListener{ dialog, id ->
                        // Nothing deleted
                    })
                alertDialog.create().show()
            }
        }
    }
    fun deleteThisMarker(idMarker:String):Boolean{
        var noProblem = true
        CoroutineScope(Dispatchers.IO).launch {
            db.collection("markers").document(idMarker).delete()
            val storage = FirebaseStorage.getInstance().reference.child("images/${idMarker}")
            storage.delete().addOnSuccessListener {
                noProblem = true
            }.addOnFailureListener{
                noProblem = false
            }
        }
        return noProblem
    }
    fun navigateBacktoList(){
        val directions = MarkerEditDirections.actionMarkerEditToListFragment()
        findNavController().navigate(directions)
    }
}