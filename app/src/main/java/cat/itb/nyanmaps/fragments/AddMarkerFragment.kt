package cat.itb.nyanmaps.fragments

import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import cat.itb.nyanmaps.MarkerViewModel
import cat.itb.nyanmaps.R
import cat.itb.nyanmaps.Tag
import cat.itb.nyanmaps.databinding.FragmentAddMarkerBinding
import cat.itb.nyanmaps.model.NyanMarker
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import java.text.DecimalFormat

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [AddMarkerFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AddMarkerFragment : Fragment() {
    // TODO: Rename and change types of parameters
    lateinit var binding: FragmentAddMarkerBinding
    private val db = FirebaseFirestore.getInstance()
    private val viewModel: MarkerViewModel by activityViewModels()



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAddMarkerBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        /*viewModel.currentMarker.observe(viewLifecycleOwner, {
            binding.latitudeText.text = DecimalFormat("#.####").format(it.latitude)
            binding.longitudeText.text = DecimalFormat("#.####").format(it.longitude)
            binding.markerImage.setImageBitmap(BitmapFactory.decodeFile(it.imagePath.path))
            binding.markerImage.rotation = 90f
        })
        binding.cameraButton.setOnClickListener {
            findNavController().navigate(R.id.action_addMarkerFragment_to_cameraFragment)
        }
        binding.addButton.setOnClickListener {
            val newMarker = viewModel.currentMarker.value!!
            newMarker.tag = binding.tagSpinner.selectedItem as Tag
            viewModel.addMarker(newMarker)
        }
        binding.tagSpinner.adapter =
            ArrayAdapter(this.requireContext(), android.R.layout.simple_spinner_item, Tag.values())
*/


        var markerColor = 0f
        var latitude:Float = 0f
        var longitude:Float = 0f
        if(viewModel.getSavedMarker()==null){
            findNavController().navigate(R.id.mapFragment)
        }
        longitude = viewModel.getSavedMarker()!!.longitude!!
        latitude = viewModel.getSavedMarker()!!.latitude!!
        binding.coordenades.text = "[${String.format("%.4f", latitude)}, ${String.format("%.4f", longitude)}]"
        binding.titleName.setText(viewModel.getSavedMarker()!!.name)

        val listColors = resources.getStringArray(R.array.color_spinner)
        val adapterSpinner = ArrayAdapter(requireContext(), android.R.layout.simple_spinner_item, listColors)
        binding.colorSelect.adapter = adapterSpinner
        binding.colorSelect.setSelection(adapterSpinner.getPosition(viewModel.textFromValue(viewModel.getSavedMarker()!!.color!!)))
        binding.colorSelect.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                when(listColors[p2]){
                    "Blue"-> markerColor = BitmapDescriptorFactory.HUE_BLUE
                    "Violet"-> markerColor = BitmapDescriptorFactory.HUE_VIOLET
                }
                //println(markerColor)
            }
            override fun onNothingSelected(p0: AdapterView<*>?) {
                markerColor = viewModel.getSavedMarker()!!.color!!
                //println(markerColor)
            }
        }
        if(viewModel.getSavedMarker()!!.image != null){
            binding.AddImage.visibility = View.GONE
            //binding.cameraImage.setImageURI(viewModel.getSavedMarker()!!.image)
            binding.cameraImage.setImageBitmap(BitmapFactory.decodeFile(Uri.parse(viewModel.getSavedMarker()!!.image!!).path))
            binding.cameraImage.rotation = 90f
        }
        binding.AddImage.setOnClickListener{
            viewModel.saveImageData(binding.titleName.text.toString(), latitude, longitude, markerColor)
            findNavController().navigate(R.id.cameraFragment)
        }
        binding.acceptButton.setOnClickListener{
            if(viewModel.getSavedMarker()!!.image!=null){
                val image = viewModel.getSavedMarker()!!.image
                val marker = NyanMarker(latitude = latitude!!, longitude = longitude!!, color =  markerColor, name = binding.titleName.text.toString(), image = "")
                viewModel.addToList(marker)
                //returnToMap(latitude, longitude, markerColor, true)
                db.collection("markers").add(marker).addOnSuccessListener{
                    println("Marcador crat amb ID: ${it.id}")
                    println("Imatge: "+marker.image)
                    val storage = FirebaseStorage.getInstance().getReference("images/${it.id}")

                    storage.putFile(Uri.parse(image))
                        .addOnSuccessListener {
                            Toast.makeText(requireContext(), "Image uploaded!", Toast.LENGTH_SHORT).show()
                        }
                        .addOnFailureListener {
                            Toast.makeText(requireContext(), "Image not uploaded!", Toast.LENGTH_SHORT).show()
                        }

                }.addOnFailureListener { e ->
                    Log.w("ERROR", "Error creant el marcador", e)
                }

                findNavController().navigate(R.id.mapFragment)
            }else{
                Toast.makeText(requireContext(),"No image added.", Toast.LENGTH_LONG).show()
            }

        }
        binding.cancelButton.setOnClickListener{
            //returnToMap(null, null, null, false)
            findNavController().navigate(R.id.mapFragment)
        }


    }
}