package cat.itb.nyanmaps.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import cat.itb.nyanmaps.MarkerViewModel
import cat.itb.nyanmaps.databinding.FragmentNyanMapBinding
import cat.itb.nyanmaps.model.NyanMarker
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
const val REQUEST_CODE_LOCATION = 100

class NyanMapFragment : Fragment(), OnMapReadyCallback {

    private lateinit var binding: FragmentNyanMapBinding
    lateinit var map: GoogleMap
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var startingPosition: LatLng
    private val viewModel: MarkerViewModel by activityViewModels()
    private val db = FirebaseFirestore.getInstance()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
/*        val rootView =  inflater.inflate(R.layout.fragment_nyan_map, container, false)
        createMap()
        updateMarkers()
        return rootView*/
        binding = FragmentNyanMapBinding.inflate((layoutInflater))
        createMap()
        viewModel.resetSave()
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this.requireActivity())
        return binding.root
    }

    fun createMap(){
        val mapFragment =
            childFragmentManager.findFragmentById(binding.map.id) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.setOnMapLongClickListener {
            println(it.latitude.toString() + ", " + it.longitude.toString())
            val directions = NyanMapFragmentDirections.actionNyanMapFragmentToAddMarkerFragment()
            viewModel.saveMarker(
                NyanMarker(
                    id = null,
                    it.latitude.toFloat(),
                    it.longitude.toFloat(),
                    image = null
                )
            )
            findNavController().navigate(directions)
        }
        createMarkers()
        enableLocation()
        getDeviceLocation()
        println("location enabled")
    }

    private fun focusLocation(location: LatLng) {
        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(location, 18f),
            5000,
            null
        )
    }

    fun createMarkers() {

        CoroutineScope(Dispatchers.IO).launch {
            db.collection("markers").addSnapshotListener(object : EventListener<QuerySnapshot> {
                override fun onEvent(value: QuerySnapshot?, error: FirebaseFirestoreException?) {
                    if (error != null) {
                        Log.e("Firestore error", error.message.toString())
                        return
                    }
                    db.collection("markers").get().addOnSuccessListener {
                        for (document in it) {
                            val newMark = document.toObject(NyanMarker::class.java)
                            newMark.id = document.id
                            println(document.id)
                            createMarker(
                                LatLng(
                                    newMark.latitude!!.toDouble(),
                                    newMark.longitude!!.toDouble()
                                ), newMark.name!!, newMark.color!!
                            )
                        }
                    }
                }
            })
        }
    }


/*    fun createMarker(){
        val coordinates = LatLng(41.4534227,2.1841046)
        val myMarker = MarkerOptions().position(coordinates).title("ITB")
        map.addMarker(myMarker)
        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(coordinates, 18f),
            5000, null)
    }*/


    fun createMarker(coordinates: LatLng, title: String, markerColor: Float) {
        val myMarker = MarkerOptions().position(coordinates).title(title)
            .icon(BitmapDescriptorFactory.defaultMarker(markerColor))
        map.addMarker(myMarker)
        //map.animateCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 18f),5000, null)
    }
   /* fun createMarker(nyanMarker: NyanMarker){
        val coordinates = LatLng(nyanMarker.latitude, nyanMarker.longitude)
        map.addMarker(MarkerOptions().position(coordinates).title("test"))
    }*/
    /*fun updateMarkers() {
        viewModel.markers.value?.map {
            createMarker(it)
        }
    }*/


    private fun requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            Toast.makeText(
                requireContext(),
                "Ves a la pantalla de permisos de l’aplicació i habilita el de Geolocalització",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE_LOCATION
            )
        }
    }

    private fun isLocationPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }

    @SuppressLint("MissingPermission")
    private fun enableLocation() {
        if (!::map.isInitialized) return
        if (isLocationPermissionGranted()) {
            map.isMyLocationEnabled = true
        } else {
            requestLocationPermission()
        }
    }

    @SuppressLint("MissingPermission")
    fun getDeviceLocation() {
        val locationResult = fusedLocationProviderClient.lastLocation
        this.activity?.let {
            locationResult.addOnCompleteListener(it) { task ->
                if (task.isSuccessful) {
                    // Set the map's camera position to the current location of the device.
                    startingPosition = LatLng(task.result.latitude, task.result.longitude)
                    focusLocation(startingPosition)
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_CODE_LOCATION -> if (grantResults.isNotEmpty() &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                map.isMyLocationEnabled = true
            } else {
                Toast.makeText(
                    requireContext(), "Accepta els permisos de geolocalització",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

/*    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        if (!::map.isInitialized) return
        if (!isLocationPermissionGranted()) {
            map.isMyLocationEnabled = false
            Toast.makeText(
                requireContext(), "Accepta els permisos de geolocalització",
                Toast.LENGTH_SHORT
            ).show()
        }
    }*/
}